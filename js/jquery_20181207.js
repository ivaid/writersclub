$(".list-group-item-action").click(function(){
    $(".active").addClass("list-group-item-action").removeClass("active");
    $(this).addClass("active").removeClass("list-group-item-action");
});
//*/
var user={
    editForm:function(id){
        do_ajax({user:id,action:"editForm"},"/api/users.php",function(resp) {
            popup.body(resp.body);
            popup.title(resp.title);
            popup.show();
        });
    },
    asmt:function(e){
        location.href="/"+e.project+"/"+e.role;
    },
    activate:function(id){
        do_ajax({user:id,action:"activate"},"/api/users.php",function(resp) {
            users.list();
        });
    },
    deactivate:function(id){
        do_ajax({user:id,action:"deactivate"},"/api/users.php",function(resp) {
            users.list();
        });
    },
    changepass:function(){
        do_ajax({form:"change-passwd"},"/api/form.php",function(resp) {
            cards.main.title("Change Password");
            cards.main.body(resp);
        });
    },
    check:{
        userid:function(e){
            var pattern="[a-z0-9\.\_]{3,50}";
            var userid=e.match(pattern);
            if(JSON.stringify(userid).length>5){
                if(userid[0]!=userid['input']){
                    $("[data=userid-status").html("<error>Invalid username,Make sure user name contains only  a-z 0-9 _ . Commas, uppercase letters and special characters are not allowed. </error>");
                } else {
                     do_ajax({userid:userid[0],action:'user-validate'},"/api/auth/register.php",function(resp) {
                         $("[data=userid-status").html(resp.status);
                     });    
                }
                
            }
        },
        passwd:function(e){
            var creds={
                pass:$(e).find("[name=pass]").val(),
                cnfpass:$(e).find("[name=cnfpass]").val()
            }
            console.log(creds);
            if (creds.pass==creds.cnfpass) {
                console.log("Passwd same");
                if (creds.pass.length<6) {
                    $("[data=pass-status]").html("<error>Password must be at least six characters long</error>");
                } else {
                    $("[data=pass-status]").html("Ok");
                }
            } else {
                console.log("Password different");
                $("[data=pass-status]").html("<error>Not matching with password</error>");
            }
        }
    },
    changerole:function(id){
        do_ajax({id:id,action:"changerole"},"/api/users.php",function(resp) {
            users.list();
        });
    }
};
var users={
    logout:function(){
        do_ajax({action:"logout"},"/api/users.php",function(resp){
            if(resp.status){
                location.reload();
            }else{
                popup.title("Logout");
                popup.body("You are not logged out due to system error. Please contact administrator for more details. Error code:USRSFNF");
                popup.show();
            }
        });
    },
    resetPassword:function(id){
        do_ajax({user:id,action:"resetPasswd"},"/api/users.php",function(resp) {
            popup.body("New password for "+resp.user+" is "+resp.newpass);
            popup.title("Password reset successful");
            popup.show();
        });
    },
    list:function(){
        do_ajax({action:"userlist"},"/api/users.php",function(resp) {
            cards.main.title(resp.heading);
            cards.main.body(resp.body);
        });
    },
    add:function(){
        do_ajax({action:"add"},"/api/users.php",function(resp) {
            cards.main.title(resp.heading);
            cards.main.body(resp.body);
        });
    },
    addNew:function(e){
        if($("[data=userid-status]").html()!="Ok"){
            $("[name=userid]").focus();
        }else if($("[data=pass-status]").html()!="Ok"){
            $("[name=pass]").focus();
        }else{
            var data={
                "userid":$("[name=userid]").val(),
                "name":$("[name=name]").val(),
                "pass":$("[name=pass]").val(),
                "email":$("[name=email]").val(),
                "phone":$("[name=phone]").val(),
                "action":"addNewUser"
            }
            do_ajax(data,"/api/users.php",function(resp) {
                users.list();
            });
        }
    },
    update:function(e){
        popup.close();
        var data={
            "id":$("[name=id]").val(),
            "name":$("[name=name]").val(),
            "email":$("[name=email]").val(),
            "phone":$("[name=phone]").val(),
            "action":"update"
        }
        
        do_ajax(data,"/api/users.php",function(resp) {
            popup.title(resp.title);
            popup.body(resp.body);
            popup.show();
            users.list();
        });
        popup.show();
    }
};
var project={
    list:function(){
        do_ajax({action:"get_projects"},"/api/projects.php",function(resp) {
            cards.main.title("Projects");
            cards.main.body(resp);
        });
    },
    update:function(id){
        do_ajax({action:"update_project",project:id},"/api/projects.php",function(resp) {
            cards.main.title("Updating Project");
            cards.main.body(resp);
        });
    },
    add:function(){
        do_ajax({action:"add_project"},"/api/projects.php",function(resp) {
            cards.main.title("Add a new Project");
            cards.main.body(resp);
        });
    },
    changeDetails(){
        var e=$(".main");
        var data={
            "action":"",
            "id":e.find("[name=id]").val(),
            "name":e.find("[name=name]").val(),
            "url":e.find("[name=url]").val(),
            "description":e.find("[name=description]").val()
        };
        //Validating
        if(data.name.length<1){
            error.prompt("Name must not be blank");
            e.find("[name=name]").focus();
        }else if(data.url.length<1){
            error.prompt("URL must not be blank");
            e.find("[name=url]").focus();
        }else if(data.description.length<1){
            error.prompt("Description must not be blank");
            e.find("[name=description]").focus();
        }else if(data.id.length==0) {
            data.action="project_add";
            console.log("Adding new project");
            do_ajax(data,"/api/projects.php",function(resp) {
                e.find("[data=card-heading]").html("Projects");
                e.find("[data=card-info]").html(resp);
                project.list();
            });
        } else {
            data.action="project_update";
            console.log("Updating project");
            do_ajax(data,"/api/projects.php",function(resp) {
                e.find("[data=card-heading]").html("Projects");
                e.find("[data=card-info]").html(resp);
                project.list();
            });
        }
        
    }
}
var roles={
    list:function(){
        do_ajax({action:"list"},"/api/roles.php",infoUpdate);
    },
    add:function(){
        do_ajax({action:"add"},"/api/roles.php",infoUpdate);
    },
    validate:{
        rolename:function(e){
            var r={rolename:$(e).val(),action:'roleExists'};
            if(r.rolename.length>3){
            do_ajax(r,"/api/roles.php",function(resp) {
               $("[data=rolename-status]").html(resp.status);
            });
            }
        }
    },
    addNew:function(e){
        var role={name:$("[name=name]").val(),description:$("[name=description]").val(),action:"AddNewRole"}
        if($("[data=rolename-status]").html()=="Ok"){
            if(role.name.length!=0){
                do_ajax(role,"/api/roles.php",infoUpdate);
            }else{
                $('[name=name]').focus();
                $("[data=rolename-status]").html("<error>Requred</error>");
            }
        }
    },

}
var permission={
    roles:{
        open:function(e){
            $(".roles-list").slideUp();
            $(".open-accordion").attr("onclick","permission.roles.open(this)").attr("class","close-accordion");
            $(e.parentNode.parentNode).find(".roles-list").slideDown();
            $(e).attr("onclick","permission.roles.hide(this)");
            $(e).attr("class","open-accordion");
        },
        hide:function(e){
            $(e.parentNode.parentNode).find(".roles-list").slideUp();
            $(e).attr("onclick","permission.roles.open(this)");
            $(e).attr("class","close-accordion");
        }
    },
    list:function(){
        cards.main.title("Manage Permissions");
        cards.main.body("List permissions by");
        var btn1=$("<button></button>");
        var btn2=$("<button></button>");
        var btn3=$("<button></button>");
        btn1.addClass("btn btn-primary btn-sm waves-effect waves-light").attr("onclick","permission.listby('user')").html("By Users");
        cards.main.append.body(btn1);
        btn2.addClass("btn btn-primary btn-sm waves-effect waves-light").attr("onclick","permission.listby('role')").html("By Role");
        cards.main.append.body(btn2);
        btn3.addClass("btn btn-primary btn-sm waves-effect waves-light").attr("onclick","permission.listby('project')").html("By Project");
        cards.main.append.body(btn3);
    },
    listby:function(e){
        do_ajax({action:"list",listby:e},api.permission,infoUpdate);
    },
    update:{
        user:function(id){
            do_ajax({user:id,action:"update",listby:"user"},api.permission,infoUpdate);
        },
        project:function(id){
            do_ajax({project:id,action:"update",listby:"project"},api.permission,infoUpdate);
        }
    },
    manage:function(e){
        var info={
            user:$(e).find("[name=user]").val(),
            project:$(e).find("[name=project]").val(),
            role:$(e).find("[name=role]").val(),
            action:'update',
            listby:'updatenow'
        }
        do_ajax(info,api.permission,function (resp){
            infoUpdate(resp);
            permission.listby('user');
        });
        return false;
    },
    revoke:function(e){
        do_ajax({id:e.id,refresh:e.refresh,action:'revoke'},api.permission,function(resp) {
            if(resp.status=="done"){
                $("[permission="+e.id+"]").remove();
            }
        });
        //permission.listby(e.refresh);
    }
}

function do_ajax(d,u,f){
    
    $.ajax({
       url:u,
       method:"post",
       data:d,
       success:f
     });

  }
var error={
    prompt:function(msg){
        alert(msg);
        console.log(msg);
    }
}
var articles={
    select:{
        project:function(){
            do_ajax({action:"listprojects"},api.articles,function(resp){
                infoUpdate(resp);
                tinymce.init({
                    selector: '.textedit',
                    menubar:false,
                    plugins: "paste image imagetools lists youtube",
                    toolbar: "styleselect | bold italic underline removeformat undo redo | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image  youtube",
                    image_list: "/api/images.php",
                    images_upload_url: '/api/upload.php',
                    statusbar:false,
                    height:400
                  });
            });
        }
    }
}

var api={
    permission:"/api/permissions.php",
    user:"/api/users.php",
    project:"/api/projects.php",
    articles:"/api/articles.php"
}
var popup={
    show:function(){
        $("#popup").css("display","flex");
        $("body").addClass("popup-open");
    },
    title:function(title){
        $("[data=heading]").html(title);        
    },
    body:function(body){
        $("[data=message]").html(body);
    },
    close:function(){
        $("#popup").css("display","none");
        $("body").removeClass("popup-open");
    }
}

var cards={
    main:{
        title:function(){
            switch(arguments.length){
                case 0:
                    return $(".main").find("[data=card-heading]").html();
                break;
                case 1:
                    $(".main").find("[data=card-heading]").html(arguments[0]);
                    return 0;
                break;
            }
        },
        body:function(){
            switch(arguments.length){
                case 0:
                    return $(".main").find("[data=card-info]").html();
                break;
                case 1:
                    $(".main").find("[data=card-info]").html(arguments[0]);
                    return 0;
                break;
            }
        },
        append:{
            body:function(e){
                $(".main").find("[data=card-info]").append(e);
            },
            title:function(e){
                $(".main").find("[data=card-heading]").append(e);
            }
        }
    },
    right:{
        title:function(){
            switch(arguments.length){
                case 0:
                    return $("#right-card").find("[data=right-title]").html();
                break;
                case 1:
                    $("#right-card").find("[data=right-title]").html(arguments[0]);
                    return 0;
                break;
            }
        },
        body:function(){
            switch(arguments.length){
                case 0:
                    return $("#right-card").find("[data=right-body]").html();
                break;
                case 1:
                    $("#right-card").find("[data=right-body]").html(arguments[0]);
                    return 0;
                break;
            }
        },
        append:{
            body:function(e){
                $("#right-card").find("[data=right-body]").append(e);
            },
            title:function(e){
                $("#right-card").find("[data=right-title]").append(e);
            }
        }
    },
    swap:function(){
        var l={
            title:cards.main.title(),
            body:cards.main.body()
        };
        cards.main.title(cards.left.title());
        cards.main.body(cards.left.body());

        cards.left.title(l.title);
        cards.left.body(l.body);
    }
}
infoUpdate=function(resp) {
    if ("main" in resp) {
        cards.main.title(resp.main.title);
        cards.main.body(resp.main.body);
    }
    if ("right" in resp) {
        cards.right.title(resp.right.title);
        cards.right.body(resp.right.body);
    }if("popup" in resp){
        popup.title(resp.popup.title);
        popup.body(resp.popup.body);
        popup.show();
    } 
}
