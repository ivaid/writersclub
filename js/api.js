var api={
    logout:"/api/logout.php",
    article:"/api/dashboard/articles.php",
    project:"/api/dashboard/projects.php",
    roles:"/api/dashboard/roles.php",
    user:"/api/dashboard/users.php"
}
var template={
    project:{
        list:"/templates/projects/list.php",
        addNew:"/templates/projects/add.php"
    },
    users:{
        list:"/templates/users/list.php"
    },
    roles:{
        list:"/templates/roles/list.php"
    },
    articles:{
        list:"/templates/articles/list.php",
        add:"/templates/articles/add.php"
    }
}