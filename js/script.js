$(".list-group-item-action").click(function(){
    $(".active").addClass("list-group-item-action").removeClass("active");
    $(this).addClass("active").removeClass("list-group-item-action");
});
var pagecomp={
    main:{
        comp:document.querySelector("#main"),
        title:{
            comp:document.querySelector("#main [data=title]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        },
        body:{
            comp:document.querySelector("#main [data=body]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        }
    },
    right:{
        comp:document.querySelector("#right"),
        title:{
            comp:document.querySelector("#right [data=title]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        },
        body:{
            comp:document.querySelector("#right [data=body]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        }
    },
    popup:{
        comp:document.querySelector("#popup"),
        title:{
            comp:document.querySelector("#popup [data=title]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        },
        body:{
            comp:document.querySelector("#popup [data=body]"),
            update:function(v){
                this.comp.innerHTML=v;
            },
            get:function(){
                return this.comp.innerHTML;
            }
        },
        show:function(){
            this.comp.style="display:block";
        },
        hide:function(){
            this.comp.style="";
        }
    },
    update:function(){
        if(this.right.title.get()==""){
            this.right.comp.style="display:none";
            this.main.comp.classList.value="col-md-12 mb-4";
        }else{
            this.right.comp.style="";
            this.main.comp.classList.value="col-md-9 mb-4";
        }
    }
}
var user={
    logout:function(){
        do_ajax({},api.logout,function(resp){
            switch(resp.status){
                case "error":
                    alert(resp.message);
                break;
                default:
                    location.reload();
                break;;
            }
        });
    }
}
var project={
    list:function(){
        if(arguments.length==1){
            data=arguments[0];
        }else{
            data={
                action:"list"
            }
        }
        do_ajax({},template.project.list,function(resp) {
            pagecomp.main.title.update("Projects");
            pagecomp.main.body.update(resp);
            pagecomp.update();
            $('.main [data=body] table').DataTable( {
                "ajax": api.project,
                "columns": [
                    { "data": "id" },
                    { "data": "name" },
                    { "data": "url" },
                    {"defaultContent":'<button class="btn btn-primary btn-sm waves-effect waves-light" onclick="project.update(this.parentNode.parentNode)">Update</button>'}
                ]
            } );
        });
    },
    update:function(e){
        var data=e.querySelectorAll("td");
        this.updateform(data[0].innerHTML);
    },
    updateform:function(e){
        do_ajax({action:"update_form",project:e},api.project,function(resp) {
            pagecomp.main.title.update("Updating project "+resp.project.name);
            pagecomp.main.body.update(resp.form);
            pagecomp.right.body.update('<div class="list-group-item list-group-item-action waves-effect" onclick="project.addForm()"><button class="btn btn-primary btn-sm waves-effect waves-light" >Add New Project</button></div>' +resp.projects.reduce(
                function(acc,proj){
                    return acc + '<div class="list-group-item list-group-item-action waves-effect" onclick="project.updateform('+proj.id+')">'+proj.name+'</div>';},""));
            pagecomp.right.title.update("Select Prject");
            pagecomp.update();

        });
    },
    changeDetails:function(e){
        var elm=e.form.querySelectorAll("[name]");
        var data={};
        elm.forEach(element => {
            data[element.name]=element.value;
        });
        data['action']="update_project";
        do_ajax(data,api.project,function(resp) {
            pagecomp.main.body.update(resp.status);
        });
    },
    addForm:function(){
        do_ajax({},template.project.addNew,function(resp) {
            pagecomp.main.title.update("Add New Project");
            pagecomp.main.body.update(resp);
            pagecomp.update();
        });
    },
    add:function(e){
        var elm=e.form.querySelectorAll("[name]");
        var data={};
        elm.forEach(element => {
            data[element.name]=element.value;
        });
        data['action']="add_project";
        do_ajax(data,api.project,function(resp) {
            pagecomp.main.title.update("Project "+ resp.project.name +" Added Successfully");
            pagecomp.main.body.update(resp.status);
            pagecomp.right.body.update('<div class="list-group-item list-group-item-action waves-effect" onclick="project.addForm()"><button class="btn btn-primary btn-sm waves-effect waves-light" >Add New Project</button></div>' +resp.projects.reduce(
                function(acc,proj){
                    return acc + '<div class="list-group-item list-group-item-action waves-effect" onclick="project.updateform('+proj.id+')">'+proj.name+'</div>';},""));
            pagecomp.right.title.update("Select Prject");
            pagecomp.update();
        });
    }
}

var users={
    list:function(){
        if(arguments.length==1){
            data=arguments[0];
        }else{
            data={
                action:"list"
            }
        }
        do_ajax({},template.users.list,function(resp) {
            pagecomp.main.title.update("Users");
            pagecomp.main.body.update(resp);
            pagecomp.update();
            $('.main [data=body] table').DataTable( {
                "ajax": api.user,
                "columns": [
                    { "data": "userid" },
                    { "data": "name" },
                    { "data": "phone" },
                    { "data": "email" },
                    {"defaultContent":'<button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.update(this.parentNode.parentNode)">Update</button><button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.disable(this.parentNode.parentNode)">Disable</button><button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.pwreset(this.parentNode.parentNode)">Reset Password</button>'}
                ]
            } );
        });
    },
    update:function(e){
        var data=e.querySelectorAll("td");
        this.updateform(data[0].innerHTML);
    },
    updateform:function(e){
        do_ajax({action:"update_form",project:e},api.project,function(resp) {
            pagecomp.main.title.update("Updating project "+resp.project.name);
            pagecomp.main.body.update(resp.form);
            pagecomp.right.body.update('<div class="list-group-item list-group-item-action waves-effect" onclick="project.addForm()"><button class="btn btn-primary btn-sm waves-effect waves-light" >Add New Project</button></div>' +resp.projects.reduce(
                function(acc,proj){
                    return acc + '<div class="list-group-item list-group-item-action waves-effect" onclick="project.updateform('+proj.id+')">'+proj.name+'</div>';},""));
            pagecomp.right.title.update("Select Prject");
            pagecomp.update();
        });
    },
    changeDetails:function(e){
        var elm=e.form.querySelectorAll("[name]");
        var data={};
        elm.forEach(element => {
            data[element.name]=element.value;
        });
        data['action']="update_project";
        do_ajax(data,api.project,function(resp) {
            pagecomp.main.body.update(resp.status);
        });
    },
    addForm:function(){
        do_ajax({},template.project.addNew,function(resp) {
            pagecomp.main.title.update("Add New Project");
            pagecomp.main.body.update(resp);
            pagecomp.update();
        });
    },
    add:function(e){
        var elm=e.form.querySelectorAll("[name]");
        var data={};
        elm.forEach(element => {
            data[element.name]=element.value;
        });
        data['action']="add_project";
        do_ajax(data,api.project,function(resp) {
            pagecomp.main.title.update("Project "+ resp.project.name +" Added Successfully");
            pagecomp.main.body.update(resp.status);
            pagecomp.right.body.update('<div class="list-group-item list-group-item-action waves-effect" onclick="project.addForm()"><button class="btn btn-primary btn-sm waves-effect waves-light" >Add New Project</button></div>' +resp.projects.reduce(
                function(acc,proj){
                    return acc + '<div class="list-group-item list-group-item-action waves-effect" onclick="project.updateform('+proj.id+')">'+proj.name+'</div>';},""));
            pagecomp.right.title.update("Select Prject");
            pagecomp.update();
        });
    }
}
var roles={
    list:function(){
        if(arguments.length==1){
            data=arguments[0];
        }else{
            data={
                action:"list"
            }
        }
        do_ajax({},template.roles.list,function(resp) {
            pagecomp.main.title.update("Roles assignments");
            pagecomp.main.body.update(resp);
            pagecomp.update();
            $('.main [data=body] table').DataTable( {
                "ajax": api.roles,
                "columns": [
                    { "data": "userid" },
                    { "data": "name" },
                    { "data": "phone" },
                    { "data": "email" },
                    {"defaultContent":'<button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.update(this.parentNode.parentNode)">Update</button><button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.disable(this.parentNode.parentNode)">Disable</button><button class="btn btn-primary btn-sm waves-effect waves-light" onclick="users.pwreset(this.parentNode.parentNode)">Reset Password</button>'}
                ]
            } );
        });
    }
}

var articles={
    list:function(){
        if(arguments.length==1){
            data=arguments[0];
        }else{
            data={
                action:"list"
            }
        }
        do_ajax({},template.articles.list,function(resp) {
            pagecomp.main.title.update("Roles assignments");
            pagecomp.main.body.update(resp);
            pagecomp.update();
            $('.main [data=body] table').DataTable( {
                "ajax": api.roles
            } );
        });
    },
    addForm:function(){
        if(arguments.length==0){
            do_ajax({action:"get_project_list",role:"Article Writer"},api.article,function(resp) {
                pagecomp.popup.show();
                pagecomp.popup.title.update(resp.title);
                pagecomp.popup.body.update(resp.body);
            });
        }else{
            do_ajax({action:"add_article_form",project:arguments[0]},api.article,function(resp) {
                console.log(resp);
                pagecomp.main.title.update(resp.title);
                pagecomp.main.body.update(resp.form);
                pagecomp.update();
                pagecomp.popup.hide();
                tinymce.init({
                    selector: '.tinymce',
                    menubar:false,
                    plugins: "image imagetools lists youtube paste twitter",
                    toolbar: "styleselect | bold italic underline removeformat undo redo | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image  youtube twitter",
                    images_upload_url: '//cdn.dinacharya.in/api/upload.php',
                    statusbar:false,
                    height:400
                  });
                  $('#fileupload').fileupload({
                        url: "/upload/"+resp.project,
                        dataType: 'json',
                        done: function (e,data) {
                        },
                        progressall: function (e, data) {
                            var progress = parseInt(data.loaded / data.total * 100, 10);
                            $('#progress .progress-bar').css(
                                'width',
                                progress + '%'
                            );
                            console.log(progress);
                        }
                    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
            });
        }
    },
    add:function(e){
        console.log(e.form.querySelectorAll("[name]"));
    }
}

function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
}
