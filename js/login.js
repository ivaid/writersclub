var api={
    login:"/api/login.php"
}
var login={
    submit:function(e){
        var a=e.form.querySelectorAll("[name]");
        var user={};
        a.forEach(element => {
            user[element.attributes.name.value]=element.value;
        });
        do_ajax(user,api.login,function(resp) {
            if("auth_stat" in resp){
                switch (resp.auth_stat) {
                    case "Login Successful":
                        window.location.reload();
                        break;                
                    default:
                        $("error").html(resp.auth_stat);
                        break;
                }
            }
        });
    }
}

function do_ajax(d,u,f){    
    $.ajax({
       url:u,
       method:"post",
       data:d,
       success:f
     });
  }