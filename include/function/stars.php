<?php
function stars($val){
    $n=floor($val);
    for($i=0;$i<$n;$i++){
        ?><span><i class="fa fa-star"></i></span><?php
    }
    if($val-$n>0){
        ?><span><i class="fa fa-star-half-o"></i></span><?php
        $n++;
    }
    for($i=$n;$i<5;$i++){
        ?><span><i class="fa fa-star-o"></i></span><?php
    }
}
?>