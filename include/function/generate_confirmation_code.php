<?php
function generate_confirmation_code($len=6,$chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"){
	$code="";
	for($i=0;$i<$len;$i++){
		$code.=$chars[rand(0,strlen($chars)-1)];
	}
	return $code;
}