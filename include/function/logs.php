<?php 
function logs($category,$activity,$location="",$user=""){
    return Array(
        "user"=>(!empty($user))?$user:($_SESSION['user']['email']??'No Login'),
        "location"=>(!empty($location))?$location:$_SERVER['REMOTE_ADDR'],
        "category"=>$category,
        "activity"=>$activity
    );
}