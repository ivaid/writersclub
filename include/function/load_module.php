<?php
function load_module($module){
	global $settings;
	$file=$settings['path']['module'].$module."/load.php";
	if(file_exists($file)){
		include $file;
	}else{
		echo "<h1>Module $module is not installed properly on this website. Please contact Web Administrator</h1>";
	}
}