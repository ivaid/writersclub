<?php
if(!class_exists("slider")){
    class slider{
        private $image=Array(),$id;
        public function __Construct($id="myCarousel")
        {
            $this->id=$id;
        }
        public function add_image($img)
        {
            $this->image[]=$img;
        }
        public function image($img=Array()){
            if (count($img)==0) {
                return $this->image;
            }else{
                $this->image=$img;
            }
        }
        public function add_text($text)
        {
            
            $this->text[]=$text;
        }
        public function text($text)
        {
            $this->text=$text;
        }
        public function render()
        {
            ob_start(); 
            ?>
    <div class="bnr">
        <div id="<?=$this->id;?>" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php for ($i=0; $i <count($this->image); $i++) { 
                    ?><li data-target="#<?=$this->id;?>" data-slide-to="<?=$i;?>" <?=($i==0)?"class=\"active\"":"";?>></li><?php 
                } ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
            <?php for ($i=0; $i <count($this->image); $i++) { 
                    ?>
                    <div class="item hover-img <?=($i==0)?"active":"";?>">
                    <a href="<?=$this->image[$i]['link']??'#';?>">
                        <img src="/uploads/images/663X467/<?=$this->image[$i]['src'];?>" alt="<?=$this->image[$i]['alt'];?>">
                    <div class="carousel-caption">
                        <div class="banner-news-heading">
                            <h3><?=$this->image[$i]['txt']??'';?></h3>
                        </div>
                    </div>
                    </a>
                </div><?php 
                } ?>
                

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#<?=$this->id;?>" data-slide="prev">
                <span class="left"></span>

            </a>
            <a class="right carousel-control" href="#<?=$this->id;?>" data-slide="next">
                <span class="right"></span>
            </a>
        </div>
    </div>
            <?php
            return ob_get_clean();
        }
    }
}