<?php
class user{
    private $info=Array(),$project=Array(),$loginstatus;
    public function __Construct(){
        if(is_array($_SESSION['user']??'')){
            $this->info=$_SESSION['user'];
            $this->project=$_SESSION['project'];
            $this->loginstatus=true;
        }else{
            $this->loginstatus=false;
        }
    }
    public function logout(){
        unset($_SESSION['user']);
        unset($_SESSION['project']);
        return true;
    }
    public function status(){
        if(!$this->loginstatus){
            return "not logged in";
        }else{
            return ($this->info['active']==1)?"active":"Not Active";
        }
    }
    public function login($user,$pass){
        global $db,$visitor;
        $data=$db[0]->select("user","*","where userid='{$user}' and pass=md5('{$pass}')",true);
        if(count($data)==1){
            $user=$data[0];
            unset($user['pass']);
            $_SESSION['user']=$user;
            $d=$db[0]->select("permission","*","where user={$user['id']}");
            if(count($d)>0){
                $_SESSION['project']=$d;
            }else{
                $_SESSION['project']=Array();
            }
            //log("Authentication","Successfully Authenticated as {$user['userid']} from {$visitor['ip']}");
            return "Login Successful";
        }else{
            //log("Authentication","Authentication failed as {$user} from {$visitor['ip']}");
            return "Your username and password does not match with our records";
        }
    }
    public function get_userinfo(){
        return $this->info;
    }
    public function get_projects(){
        return $this->project;
    }
    public function update(){
        global $db,$visitor;
        $data=$db[0]->select("user","*","where id={$this->info['id']}");
        if(count($data)==1){
            $user=$data[0];
            unset($user['pass']);
            $_SESSION['user']=$user;
            $d=$db[0]->select("permission","*","where user={$user['id']}");
            if(count($d)>0){
                $_SESSION['project']=$d;
            }else{
                $_SESSION['project']=Array();
            }
            $this->project=$_SESSION['project'];
            $this->info=$_SESSION['user'];
        }
    }
}