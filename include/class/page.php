<?php
class page{
    private $title,$css=Array(),$js=Array(),$html="",$bodyAttr;
    public function __Construct($title=""){
        $this->title=$title;
    }
    public function css($file=""){
        if($file==""){
            return $this->css;
        }else{
            $this->css[]=$file;
        }
    }
    public function js($file=""){
        if($file==""){
            return $this->js;
        }else{
            $this->js[]=$file;
        }
    }
    public function body($attr=""){
        $this->bodyAttr=$attr;
    }
    public function addHTML($html){
        $this->html.=$html;
    }
    public function render(){
        $html="";
        $html.="<!DOCTYPE html><html lang=en-us><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><title>$this->title</title>";
        foreach ($this->css as $value) {
            $html.="<link href='{$value}' rel=stylesheet>";
        }
        $html.="</head><body {$this->bodyAttr}>".$this->html."</body>";
        foreach ($this->js as $value) {
            $html.="<script src='$value'></script>";
        }
        return $html;
    }
}