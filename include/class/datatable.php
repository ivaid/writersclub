<?php
class datatable{
  public $table=Array(),$js=Array("jquery-3.1.1.min.js","jquery.dataTables.min.js","dataTables.bootstrap.min.js"),$css=Array("dataTables.bootstrap.min.css","dataTables.foundation.min.css","dataTables.jqueryui.min.css","dataTables.semanticui.min.css","jquery.dataTables.min.css"),$action=Array();
  public $jsbase="//cdn.avriq.com/js/",$cssbase="//cdn.avriq.com/css/",$id="tbl1";
  function __construct($data) {
    $this->table=$data;
  }
  public function render() {
    ob_start();
    ?>
    <table id="<?=$this->id; ?>">
      <thead>
        <tr>
          <?php for ($i=0; $i <count($this->table['fields']); $i++) {
            echo "<th>{$this->table['fields'][$i]}</th>";
          }
          for ($i=0; $i <count($this->action) ; $i++) {
            ?>
            <th><?=$this->action[$i]; ?></th>
      <?php
          }  ?>
        </tr>
      </thead>
      <tbody>
        <?php for ($i=0; $i <$this->table['rows'] ; $i++) {
          ?>
          <tr>
            <?php
            for ($j=0; $j < count($this->table['fields']); $j++) {
              ?>
              <td><?=$this->table[$i][$j]; ?></td>
              <?php
            }
             for ($k=0; $k <count($this->action) ; $k++) {
               ?><td onclick="<?=$this->action[$k]; ?>(<?=$this->table[$i][0]; ?>)"><?=$this->action[$k]; ?></td><?php
             }
             ?>
          </tr>
          <?php
        } ?>
      </tbody>
    </table>
    <?php
    return ob_get_clean();
  }
  public function js(){
    $js=$this->js;
    for ($i=0; $i <count($js) ; $i++) {
      echo "<script src='{$this->jsbase}{$js[$i]}'></script>";
    }
  }
  public function css(){
    $css=$this->css;
    for ($i=0; $i <count($css) ; $i++) {
      echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->cssbase}{$css[$i]}\" />\n";
    }
  }
  public function action($action)
  {
    $this->action[]=$action;
  }
  public function id($id)
  {
    $this->id=$id;
  }
}
