        
<?php 
class message{
	private $head,$body,$address,$copyright,$social=Array(),$unsubscribe=false;
	public function __Construct($head,$body){
		$this->head=$head;
		$this->body=$body;
	}
	public function social($service,$link){
		$this->social[$service]=$link;
	}
	public function address($val){
		$this->address=$val;
	}
	public function copyright($val){
		$this->copyright=$val;
	}
	public function unsubscribe($val){
		$this->unsubscribe=$val;
	}
	public function render(){
		$heading=$this->head;
		$body=$this->body;
		$unsubscribe=$this->unsubscribe;
		$address=$this->address;
		ob_start();
		include "/home/avriqcom/public_html/newsletter/emailer2/index.php";
		
		$html=ob_get_clean();
		return $html;
	}
}