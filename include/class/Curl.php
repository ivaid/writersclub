<?php
class Curl
{
	private $ch;
    private $response = false;
    public function __construct($url, array $options = array())
    {
        $this->ch = curl_init($url);
        foreach ($options as $key => $val) {
            curl_setopt($this->ch, $key, $val);
        }
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
    }
    public function getResponse()
    {
         if ($this->response) {
             return $this->response;
         }
        $response = curl_exec($this->ch);
        $error    = curl_error($this->ch);
        $errno    = curl_errno($this->ch);
        if (is_resource($this->ch)) {
            curl_close($this->ch);
        }
        if (0 !== $errno) {
            throw new \RuntimeException($error, $errno);
        }
        return $this->response = $response;
    }
    public function __toString()
    {
        return $this->getResponse();
    }
}