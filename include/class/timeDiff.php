<?php

class timeDiff{
	private $dt,$ago;
	public function pluralize( $count, $text ) 
	{ 
	    return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
	}
	public function ago()
	{
	    $interval = date_create('now')->diff( $this->dt );
	    $suffix = ( $interval->invert ? ' ago' : '' );
	    if ( $v = $interval->y >= 1 ) return $this->pluralize( $interval->y, 'year' ) . $suffix;
	    if ( $v = $interval->m >= 1 ) return $this->pluralize( $interval->m, 'month' ) . $suffix;
	    if ( $v = $interval->d >= 1 ) return $this->pluralize( $interval->d, 'day' ) . $suffix;
	    if ( $v = $interval->h >= 1 ) return $this->pluralize( $interval->h, 'hour' ) . $suffix;
	    if ( $v = $interval->i >= 1 ) return $this->pluralize( $interval->i, 'minute' ) . $suffix;
	    return $this->pluralize( $interval->s, 'second' ) . $suffix;
	}
	public function print($datetimeISO){
		echo $this->get($datetimeISO);		
	}
	public function get($datetimeISO){
		$this->dt=new DateTime($datetimeISO);
		return $this->ago();
	}
}