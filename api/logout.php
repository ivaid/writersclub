<?php
$resp=Array();
$resp['status']=$u->logout()?"done":"error";
if($resp['status']=="error")$resp['message']="You are unable to logout from this computer. Please contact website administrator if the problem persists.";
header('Content-Type: application/json');
echo json_encode($resp);