<?php 
$admin=[
    
["link"=>"/",
"icon"=>"fa-user",
"text"=>"Profile"],

["link"=>"javascript=>void(0)",
"text"=>"Project",
"onclick"=>"project.list()",
"icon"=>"fa-pie-chart"],

["link"=>"javascript=>void(0)",
"text"=>"Users",
"onclick"=>"users.list()",
"icon"=>"fa-users"],

["link"=>"javascript=>void(0)",
"text"=>"Roles",
"onclick"=>"roles.list()",
"icon"=>"fa-tasks"],

["link"=>"javascript=>void(0)",
"text"=>"Articles",
"onclick"=>"articles.list()",
"icon"=>"fa-lock"],

["link"=>"javascript=>void(0)",
"text"=>"Logout",
"onclick"=>"users.logout()",
"icon"=>"fa-sign-out"]
];


header('Content-Type: application/json');
echo json_encode($admin);