<?php
$resp=Array();
$action=$_REQUEST['action']??'list';
$db[0]->mode(2);
global $project,$role;
$project=$db[0]->select("project","id,name");
$role=$db[0]->select("role","id,name");
$permissions=
array_map(
    function ($a){
        global $project,$role;
        return Array(
            'id'=>$a['project'],
            'project'=>$project[$a['project']]['name'],
            'role'=>$role[$a['role']]['name']);
        },
        $_SESSION['project']
    );
$id="";
foreach ($permissions as $key => $value) {
    if($value['id']==$_REQUEST['project']){
        $id=$value['id'];
    }
    
}
    //$id=array_reduce($permissions,function($a,$b){return $a. ($b['id']==$_REQUEST['project'])?$b['id']:"";},"");
switch ($action) {
    case 'list':
        $resp['data']=$db[0]->select("project");        
    break;
    case "update_form":
        $resp['projects']=$db[0]->select("project","*");
        $proj=$db[0]->select("project","*","where id={$_REQUEST['project']}");
        $resp['project']=Array("id"=>$proj[0]['id'],"name"=>$proj[0]['name']);
        ob_start();
        include $settings['path']['templates']."/projects/update.php";
        $form=ob_get_clean();
        $resp['form']=$form;
        $resp['input']=$_REQUEST;
    break;;
    case "update_project":
        $resp['input']=$_REQUEST;
        $project=$db[0]->resa($_REQUEST);
        unset($project['action']);
        $db[0]->update("project",$project," where id={$project['id']}");
        $resp['status']="The project details has been updated successfully.";
    break;;
    case "add_article_form":
        if(isset($project[$id])){
            $resp['project']=$id;
            $resp['title']="Adding new article in ".$project[$id]['name'];
            ob_start();
            include $settings['path']['templates']."/articles/add.php";
            $form=ob_get_clean();
            $resp['form']=$form;
        }else{
            $resp['title']="Permission Denied";
            $resp['form']="You are not authorized to add an article.";
            $resp['body']=trace($permissions,true);
        }
    break;;
    case "get_project_list":
        $resp['title']="Select Project";
        $resp['session']=$permissions;
        $resp['body']="<div class='select-popup-btn'>".array_reduce($permissions,function($a,$b){
//            echo $a;
            return ($b['role']==$_REQUEST['role'])?$a ."<button onclick='articles.addForm({$b['id']})'>".$b['project']."</button>":$a ."";
        },"")."</div>"; //*/
    break;;
    default:
        # code...
        break;
}

header('Content-Type: application/json');
echo json_encode($resp);