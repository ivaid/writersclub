<?php
$resp=Array();
$action=$_REQUEST['action']??'list';
switch ($action) {
    case 'list':
        $resp['data']=$db[0]->select("project");        
    break;
    case "update_form":
        $resp['projects']=$db[0]->select("project","*");
        $proj=$db[0]->select("project","*","where id={$_REQUEST['project']}");
        $resp['project']=Array("id"=>$proj[0]['id'],"name"=>$proj[0]['name']);
        ob_start();
        include $settings['path']['templates']."/projects/update.php";
        $form=ob_get_clean();
        $resp['form']=$form;
        $resp['input']=$_REQUEST;
    break;;
    case "update_project":
        $resp['input']=$_REQUEST;
        $project=$db[0]->resa($_REQUEST);
        unset($project['action']);
        $db[0]->update("project",$project," where id={$project['id']}");
        $resp['status']="The project details has been updated successfully.";
    break;;
    case "add_project":
        $resp['input']=$_REQUEST;
        $project=$db[0]->resa($_REQUEST);
        unset($project['action']);
        $resp['project']=$project;
        $db[0]->insert("project",$project,true);
        $resp['status']="The project details has been added successfully.";
        $resp['projects']=$db[0]->select("project","*");
    break;;
    default:
        # code...
        break;
}

header('Content-Type: application/json');
echo json_encode($resp);