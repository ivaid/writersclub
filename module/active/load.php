<?php

$style=[
    "https://fonts.googleapis.com/css?family=Alex+Brush|Italianno|Mr+Dafoe|Norican|Pinyon+Script",
    "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css",
    "/css/jquery.dataTables.min.css",
    '/css/jquery.fileupload.css',
    "/css/mdb.min.css",
    "/css/template-style.min.css",
    "/css/style.css"
];
include "action/menuitems.php";
include "dashboard/head.php";
include "dashboard/layout.php";
include "dashboard/js.php";