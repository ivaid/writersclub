
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard: The Writer's Club</title>
    <!-- Your custom styles (optional) -->
    <link rel="apple-touch-icon" sizes="57x57" href="/logo/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/logo/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/mdb.min.css" rel="stylesheet">
    <link href="/css/template-style.min.css" rel="stylesheet">
    <link media="screen" href="https://fonts.googleapis.com/css?family=Alex+Brush|Italianno|Mr+Dafoe|Norican|Pinyon+Script" rel="stylesheet">
    <link media="screen" rel="stylesheet" href="/css/style.css">
    <title>The Writer's Club</title>
</head>
<body class="grey lighten-3"><header>
<div class="sidebar-fixed position-fixed">

    <a class="logo-wrapper waves-effect">
        <img src="/logo/logo.png" class="img-fluid">
    </a>

    <div class="list-group list-group-flush">
            <a href="/"   class="list-group-item active waves-effect">
            <i class="fa fa-user mr-3"></i>Profile        </a>
            <a href="javascript:void(0)" onclick=project.list()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-pie-chart mr-3"></i>Project        </a>
            <a href="javascript:void(0)" onclick=users.list()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-users mr-3"></i>Users        </a>
            <a href="javascript:void(0)" onclick=roles.list()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-tasks mr-3"></i>Roles        </a>
            <a href="javascript:void(0)" onclick=permission.list()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-lock mr-3"></i>Permissions        </a>
            <a href="javascript:void(0)" onclick=articles.select.project()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-lock mr-3"></i>Articles        </a>
            <a href="javascript:void(0)" onclick=users.logout()  class="list-group-item list-group-item-action waves-effect">
            <i class="fa fa-sign-out mr-3"></i>Logout        </a>
        </div>
</div>
</header><main class="mx-lg-5">
    <div class="container-fluid mt-4">
        <div class="card mb-4 wow fadeIn">
            <div class="card-body d-sm-flex justify-content-between">
                <h4 class="mb-2 mb-sm-0 pt-1">
    <a href="/">Writer's Club</a>
    <span>/</span>
    <span>Dashboard</span>
</h4>
<form class="d-flex justify-content-center">
    <!-- Default input -->
    <input type="search" placeholder="Type your query" aria-label="Search" class="form-control">
    <button class="btn btn-primary btn-sm my-0 p" type="submit">
        <i class="fa fa-search"></i>
    </button>

</form>            </div>
        </div>
        <div class="row wow fadeIn">
            <div class="col-md-9 mb-4">
    <div class="card main">
        <div class="card-header text-center" data=card-heading>
            Dashboard
        </div>
        <div class="card-body" data=card-info>
        <div class="project-header">
        	<ul>
        		<li>pro</li>
        		<li>A/C</li>
        		<li>Action</li>
        	</ul>
        </div>
        </div>
    </div>
</div>            <div class="col-md-3 mb-4">
    <div class="card" id=right-card>
        <div class="card-header text-center" data=right-title>
            User Info
        </div>
        <div class="card-body" data=right-body>
            <div class="list-group list-group-flush">
                <div class="list-group-item list-group-item-action waves-effect">
                <i class="fa fa-user"></i>    
                    Om Prakash Tiwari                </div>
                <div class="list-group-item list-group-item-action waves-effect">
                <i class="fa fa-mail"></i>    
                    optiwari@ivaid.com                </div>
                <div class="list-group-item list-group-item-action waves-effect">
                <i class="fa fa-phone"></i>    
                    +91 9311150364                </div>
                <div class="list-group-item list-group-item-action waves-effect" onclick="user.changepass()">
                    Change Password
                </div>
            </div>
        </div>
    </div>
</div>        </div>
    </div>
</main><div id="popup" class=popup>
    <div class=popup-container>
        <div class="heading">
            <div class="title" data=heading></div>
            <button class=popup-close onclick=popup.close()>
                <span></span><span></span>
            </button>
        </div>
        <div class="body">
            <div class="message" data=message></div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.dinacharya.in/js/tinymce/tinymce.min.js"></script>
<script src="https://unpkg.com/hyperhtml@latest/min.js"></script>
<script src="/js/jquery.js"></script>