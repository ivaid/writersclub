<header>
<div class="sidebar-fixed position-fixed">

    <a class="logo-wrapper waves-effect">
        <img src="/logo/logo.png" class="img-fluid">
    </a>
    <div class="list-group list-group-flush">
        <?php
        foreach ($menu as $item) {
            ?>
                <a href="<?=$item['link']; ?>" <?=(isset($item['onclick']))?"onclick=".$item['onclick']."()":"";?>  class="list-group-item <?=$item['class']??'list-group-item-action';?> waves-effect">
                    <i class="fa <?=$item['icon']??''; ?> mr-3"></i><?=$item['text'];?>
                </a>
            <?php 
        }
        ?>
    </div>
</div>
</header>
