<body class="grey lighten-3">
    <?php include "menubar.php"; ?>
    <main class="mx-lg-5">
        <div class="container-fluid mt-4">
            <div class="row wow fadeIn">
                <div class="col-md-9 mb-4" id=main>
                    <div class="card main">
                        <div class="card-header text-center" data=title></div>
                        <div class="card-body" data=body>
                        <?php trace($_REQUEST); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-4" id=right>
                    <div class="card" id=right-card>
                        <div class="card-header text-center" data=title></div>
                        <div class="card-body" data=body></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div id="popup" class=popup>
        <div class=popup-container>
            <div class="heading">
                <div class="title" data=title></div>
                <button class=popup-close onclick=pagecomp.popup.hide()>
                    <span></span><span></span>
                </button>
            </div>
            <div class="body">
                <div class="message" data=body></div>
            </div>
        </div>
    </div>
