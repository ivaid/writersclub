<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="/logo/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/logo/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/logo/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/logo/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/logo/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/logo/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/logo/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/logo/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/logo/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/logo/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/logo/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/logo/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/logo/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/logo/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Alex+Brush|Italianno|Mr+Dafoe|Norican|Pinyon+Script" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
    <title>The Writer's Club</title>
</head>
<body>

<div class="container">
    <section id="wrapper">
        <div class="login-register">
            <div class="login-box card">
                <div class="logo1">
                    <img src="/logo/logo.png">
                </div>
                <div class="card-body">
                    <div class="login-with-passwd-frm">
                        <form class="form-horizontal form-material" id="loginformpasswd" method="post" autocomplete="off">
                            <h3 class="box-title m-b-20">Authentication Required</h3>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label>Username</label>
                                    <input class="form-control" name="user" type="text" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="pass" placeholder="Password"> </div>
                            </div>
                            <div class="form-group text-center m-t-20">
                                <div class="col-xs-12">
                                    <input type="hidden" name="action" value="login">
                                    <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="button" onclick="login.submit(this)">Log In</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <error data=error></error>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="/js/jquery-3.3.1.js"></script>
<script src="/js/hyperHTML.js"></script>
<script src="/js/login.js"></script>
</body>
</html>