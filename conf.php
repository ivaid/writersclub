<?php
//Starting session
session_start();
#$mode="production";
$mode="debug";
//Truning on error reporting
if($mode=="debug"){
    ini_set('display_errors', 'On');
}
//preconfig
$settings=include "config.inc.php";
foreach ($settings['function'] as $key => $function) {
    include $settings['path']['function'].$function.".php";
}

spl_autoload_register("load_class");
$db=Array();
for($i=0;$i<count($settings['database']);$i++){
    $db[$i]=new database($settings['database'][$i]);
    $db[$i]->mode(1);
}
$u=new user();
$visitor=Array(
    "ip"=>$_SERVER['REMOTE_ADDR'],
    "useragent"=>$_SERVER['HTTP_USER_AGENT']
);
$siterole=Array("User","Administrator");