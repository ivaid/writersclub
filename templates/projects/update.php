<?php
$p=$proj[0];
?>
<form method="post">
    <div class="form-group">
        <label for="">Name</label>
        <input class="form-control" type="text" name="name" value="<?=$p['name'];?>">
    </div>

    <div class="form-group">
        <label for="">URL</label>
        <input class="form-control" type="text" name="url" value="<?=$p['url'];?>">
    </div>

    <div class="form-group">
        <label for="">Description</label>
        <input class="form-control" type="text" name="description" value="<?=$p['description'];?>">
    </div>
    <input type="hidden" name="id" value="<?=$p['id'];?>">
    <div class="form-group">
        <button type=button class="form-control button btn-primary" onclick="project.changeDetails(this)">Save Changes</button>
    </div>
</form>