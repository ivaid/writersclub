<form method="post" class="article-form">
    <div class="form-group">
        <label for="">Title</label>
        <input class="form-control" type="text" name="title">
    </div>
    <?php if($id==1){ ?>
    <div class="form-group">
        <label for="">Title (in English)</label>
        <input class="form-control" type="text" name="engtitle">
    </div>
    <?php } ?>

    <div class="form-group full-w">
        <label for="">Article</label>
        <textarea class="form-control tinymce" name=article></textarea>
    </div>

    <div class="thumb-image-show">
        <label>Thumb Image</label>
        <!-- <img src="images/thumb-image.jpeg" alt=""> -->
        <div class="thumb-image-block">
        <input type="file" name="file" id=fileupload class="form-control" onchange="readURL(this);">
        <img id="blah" src="images/thumb-image.png" alt="your image" />
        <!-- <span>Upload Thumb Image</span> -->
        </div>
    </div>
    
    <div class="form-group full-w-8">
        <label for="">Summary</label>
        <textarea class="form-control"></textarea>
    </div>

    <div class="form-group full-w-8">
        <label for="">Keywords</label>
        <input class="form-control" type="text" name="keywords">
    </div>
    <input type="hidden" name="id" value="<?=$p['id'];?>">
    <div class="form-group full-w-8">
        <label></label>
        <button type=button class="form-control button btn-primary" onclick="articles.add(this)">Save</button>
    </div>
</form>