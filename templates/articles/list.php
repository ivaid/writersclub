<div class="add-btn">
    <button class="btn btn-primary btn-sm waves-effect waves-light" onclick="articles.addForm()">Add New Article</button>
</div>
<table id="example" class="display" style="width:100%">
    <thead>
        <tr>
            <th>id</th>
            <th>project</th>
            <th>Section</th>
            <th>article title</th>
            <th>Written</th>
            <th>published/rejected</th>
            <th>trending</th>
            <th>Discussions</th>
            <th>Shared</th>
        </tr>
    </thead>
</table>