<div class="add-btn">
    <button class="btn btn-primary btn-sm waves-effect waves-light" onclick="project.addForm()">Add New Project</button>
</div>
<table id="example" class="display" style="width:100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>User</th>
            <th>Project</th>
            <th>Roles</th>
            <th>Action</th>
        </tr>
    </thead>
</table>